package settings;

/**
 * Global variables
 * 
 * @author Jonathan
 * 
 */
public class Global
{
    public final static int numWikiPages = 5706070; // number of pages in links-simple-sorted.txt
    // public final static int numWikiPages = 5716737; //number of pages in title-sorted.txt
    public final static String linksFileLocation = "C:\\Temp\\Wikipedia\\links-simple-sorted.txt";
    public final static String titleFileLocation = "C:\\Temp\\Wikipedia\\titles-sorted.txt";
    public final static String smallFileLocation = "C:\\Temp\\Wikipedia\\";
    public final static String startFileLocation = "C:\\Temp\\Wikipedia\\startFileNum.txt";
    public final static int numPagesPerFile = 1000;
    public final static int linksThreshold = 75;
    public static int BFSPagesReached = 0;

}
