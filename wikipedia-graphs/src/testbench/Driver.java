package testbench;

import graph.GraphFile;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

import util.BFS;
import util.DFS;

/**
 * Driver used to run single tests
 * 
 * @author Jonathan
 * 
 */

public class Driver {
	
	public static void main(String args[]){
		GraphFile gf = new GraphFile(false);

	// RANDOM
	int sampleSize = 1000;
	int seed = 94;

	// DEMO
	// should take from user input
	String wikiPage1 = "Hawaii";
	String wikiPage2 = "New Jersey";
	int a = gf.getPageNum(wikiPage1);
	int b = gf.getPageNum(wikiPage2);

	System.out.println("Running BFS N Levels...");
	int NLevels = 1;
	// List<Integer> list = BFS.BFSCommonNLevels(gf,a,b,NLevels);
	List<Integer> list = DFS.DFSCommonNLevels(gf, a, b, NLevels); // choose one
	for (int y : list)
	    System.out.print(gf.getPageName(y) + ", ");
	System.out.println();

	// print out the path
	LinkedList<Integer> lli = BFS.findPath(gf, a, b);
	System.out.print("Path found (" + lli.size() + "): ");
	for (int y : lli)
	    System.out.print(gf.getPageName(y) + " ||| ");
	System.out.println();

	// DATA COLLECTION
	// DataCollector.BFSDFSNLevels(gf,sampleSize,seed);
	// DataCollector.BFSFindPath(gf,sampleSize,seed);

    }

    /**
     * Removes duplicates from list and sorts it
     * 
     * @param list
     *            Original list
     * @return Sorted list
     */
    public static List<Integer> removeDuplicatesAndSort(List<Integer> list)
    {
	Collections.sort(list);

	ListIterator<Integer> li = list.listIterator();
	int current = li.next();
	int next;
	while (li.hasNext())
	{
	    next = li.next();
	    if (current == next)
		li.remove();
	    else
		current = next;
	}
	return list;
    }
}
