package testbench;

import graph.GraphFile;

/**
 * Run this to break the large file into smaller files so the program can run graph algorithms on it.
 * 
 * @author Jonathan
 * 
 */
public class CreateSmallFiles
{

    /**
     * @param args
     */
    public static void main(String[] args)
    {
	System.out.println("Breaking up files...");
	GraphFile g = new GraphFile(true);
	System.out.println("Done.");
    }

}
