package graph;

import java.util.ArrayList;

/**
 * Methods used by GraphFile
 * 
 * @author Jonathan
 * 
 */
public class GraphHelper
{

    /**
     * Given an arraylist of number, perform a binary search
     * 
     * @param ali
     * @param key
     * @return The number that is closest without going over
     */
    public static int binarySearch(ArrayList<Integer> ali, int key)
    {
	int min = 0;
	int max = ali.size() - 1;
	int mid = 0;

	while (min != max)
	{
	    mid = (min + max) / 2;

	    if (ali.get(mid) == key)// direct
		return ali.get(mid);
	    else if (key > ali.get(mid) && key < ali.get(mid + 1))// found what it is between
		return ali.get(mid);
	    else if (ali.get(mid) > key)
	    { // resize
		max = mid - 1;
	    }
	    else if (ali.get(mid) < key)
	    { // resize
		min = mid + 1;
	    }
	}
	return ali.get(min);
    }
}
