package ui;

/**
 * Constants used by GraphViewer
 * @author Jonathan
 *
 */
public interface GraphViewerConstants
{
    public static final String PAGE_TITLE = "Page Title", PAGE_NUM = "Page Number";
}
